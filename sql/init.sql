create table if not exists persons (
    id serial primary key,
    name varchar(250) not null,
    birth_date timestamp not null,
    user_id integer
);

create table if not exists users (
    id serial primary key,
    username varchar(250) not null,
    password varchar(250) not null,
    person_id integer
);

create table if not exists colors (
    id serial primary key,
    color varchar(250) not null
);

create table if not exists cats (
    id serial primary key,
    name varchar(250) not null,
    birth_date timestamp,
    breed varchar(250),
    color_id integer,
    owner_id integer,
    constraint fk_cat_owner_id foreign key (owner_id) references persons,
    constraint fk_cat_color_id foreign key (color_id) references colors
);

create table if not exists cat_friends (
    cat_id integer not null,
    other_cat_id integer not null,
    constraint fk_cat_friends_cat_id foreign key (cat_id) references cats,
    constraint fk_cat_friends_other_cat_id foreign key (other_cat_id) references cats,
    primary key (cat_id, other_cat_id)
);
