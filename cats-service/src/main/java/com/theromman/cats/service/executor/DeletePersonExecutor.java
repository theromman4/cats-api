package com.theromman.cats.service.executor;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.utils.ArgumentsSource;
import com.theromman.cats.service.utils.ConsoleCommand;

public class DeletePersonExecutor extends ExecutorChain {
    @Override
    public void execute(ArgumentsSource argumentsSource) {
        if (getSupportedCommand() == argumentsSource.getConsoleCommand()) {
            CatsApplicationContext.getInstance().getPersonsConsoleAdapter().deletePerson(argumentsSource);
        } else {
            executeNext(argumentsSource);
        }
    }

    @Override
    protected ConsoleCommand getSupportedCommand() {
        return ConsoleCommand.DELETE_PERSON;
    }
}
