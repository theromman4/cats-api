package com.theromman.cats.service.services;

import static com.theromman.cats.service.utils.ConsoleCommandAttribute.BIRTH_DATE;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.CAT_ID;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.ID;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.NAME;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.OWNER_ID;

import com.theromman.cats.data.model.Person;
import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.utils.ArgumentsSource;
import java.time.LocalDate;

public class PersonsConsoleAdapterImpl implements PersonsConsoleAdapter {

    private final PersonsService personsService;

    public PersonsConsoleAdapterImpl() {
        this.personsService = CatsApplicationContext.getInstance().getPersonsService();
    }

    @Override
    public Person createPerson(ArgumentsSource arguments) {
        String name = arguments.getRequiredParameter(NAME);
        LocalDate birthDate = arguments.getRequiredParameterDate(BIRTH_DATE);
        return personsService.createPerson(name, birthDate);
    }

    @Override
    public void deletePerson(ArgumentsSource arguments) {
        Long id = arguments.getRequiredParameterLong(ID);
        personsService.deletePerson(id);
    }

    @Override
    public void removeCat(ArgumentsSource arguments) {
        Long catId = arguments.getRequiredParameterLong(CAT_ID);
        Long ownerId = arguments.getRequiredParameterLong(OWNER_ID);
        personsService.removeCat(catId, ownerId);
    }

    @Override
    public void adoptCat(ArgumentsSource arguments) {
        Long catId = arguments.getRequiredParameterLong(CAT_ID);
        Long ownerId = arguments.getRequiredParameterLong(OWNER_ID);
        personsService.addCat(catId, ownerId);
    }
}
