package com.theromman.cats.service.test_training;

public class SquareCalculator {
    public double calculate(double first) {
        return first * first;
    }
}
