package com.theromman.cats.service.services;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Color;
import com.theromman.cats.data.model.Person;
import com.theromman.cats.data.model.enums.ColorType;
import com.theromman.cats.data.repository.CrudRepository;
import com.theromman.cats.service.context.CatsApplicationContext;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class CatsServiceImpl implements CatsService {
    private final CrudRepository<Long, Cat> catsRepository;
    private final PersonsService personsService;
    private final ColorsService colorsService;

    public CatsServiceImpl() {
        this.catsRepository = CatsApplicationContext.getInstance().getCatsRepository();
        this.personsService = CatsApplicationContext.getInstance().getPersonsService();
        this.colorsService = CatsApplicationContext.getInstance().getColorsService();
    }

    @Override
    public Cat createCat(
        String name,
        Optional<String> colorName,
        Optional<Long> ownerId,
        Optional<LocalDate> birthDate,
        Optional<String> breed,
        Optional<List<Long>> friendsIds
    ) {
        Optional<Color> foundColorOptional = colorName.isPresent() ? colorsService.findByColor(colorName.get()) : Optional.empty();
        Optional<Person> owner = ownerId.map(personsService::findById);
        Cat cat = new Cat().setName(name);
        birthDate.map(cat::setBirthDate);
        breed.map(cat::setBreed);
        if (foundColorOptional.isPresent()) {
            cat.setColor(foundColorOptional.get());
        } else {
            var savedColor = colorsService.save(new Color().setColor(ColorType.valueOf(colorName.get())));
            cat.setColor(savedColor);
        }
        foundColorOptional.map(cat::setColor);
        owner.map(cat::setOwner);
        var savedCat = catsRepository.save(cat);
        if (friendsIds.isPresent()) {
            for (Long id : friendsIds.get()) {
                makeFriend(savedCat.getId(), id);
            }
        } return savedCat;
    }

    @Override
    public Person changeOwner(Long ownerId, Long catId) {
        Person owner = personsService.findById(ownerId);
        Cat cat = findById(catId);
        cat.setOwner(owner);
        catsRepository.save(cat);
        return owner;
    }

    @Override
    public void unfriendCat(Long catId, Long friendId) {
        Cat cat = findById(catId);
        Cat foundFriend = cat.getFriends().stream().filter(it -> Objects.equals(it.getId(), friendId)).toList().get(0);
        cat.getFriends().removeIf(it -> Objects.equals(it.getId(), friendId));
        foundFriend.getFriends().removeIf(it -> Objects.equals(it.getId(), cat.getId()));
        catsRepository.save(cat);
        catsRepository.save(foundFriend);
    }

    @Override
    public Cat updateCat(
        Long id,
        Optional<String> name,
        Optional<LocalDate> birthDate,
        Optional<String> breed,
        Optional<Long> ownerId
    ) {
        Optional<Person> owner = ownerId.map(personsService::findById);
        Cat cat = findById(id);
        name.map(cat::setName);
        birthDate.map(cat::setBirthDate);
        breed.map(cat::setBreed);
        owner.map(cat::setOwner);
        catsRepository.save(cat);
        return cat;
    }

    @Override
    public void printAllCats() {
        List<Cat> cats = catsRepository.findAll();
        System.out.println(cats);
    }

    @Override
    public Cat addOwner(Long catId, Long ownerId) {
        Cat cat = findById(catId);
        Person owner = personsService.findById(ownerId);
        if (cat.getOwner() != null) throw new RuntimeException("Can not set owner, already exists!");
        cat.setOwner(owner);
        return cat;
    }

    @Override
    public Cat findById(Long id) {
        return catsRepository
            .findById(id)
            .orElseThrow(() -> new RuntimeException("Cat with this id is not found " + id));
    }

    @Override
    public void deleteCat(Long catId) {
        var foundCat = findById(catId);
        foundCat.getFriends().clear();
        foundCat.getFriendOf().clear();
        catsRepository.delete(foundCat);
    }

    @Override
    public void makeFriend(Long catId, Long friendId) {
        Cat cat1 = findById(catId);
        Cat cat2 = findById(friendId);
        cat1.getFriends().add(cat2);
        cat2.getFriends().add(cat1);
        catsRepository.save(cat1);
        catsRepository.save(cat2);
    }
}
