package com.theromman.cats.service.services;

import com.theromman.cats.data.model.Color;
import com.theromman.cats.data.repository.CrudRepository;
import com.theromman.cats.service.context.CatsApplicationContext;
import java.util.Optional;

public class ColorsServiceImpl implements ColorsService {

    private final CrudRepository<Long, Color> colorsRepository;

    public ColorsServiceImpl() {
        colorsRepository = CatsApplicationContext.getInstance().getColorsRepository();
    }

    @Override
    public Optional<Color> findByColor(String colorName) {
        return colorsRepository.findByFieldValue("color", colorName);
    }

    @Override
    public Color save(Color color) {
        return colorsRepository.save(color);
    }
}
