package com.theromman.cats.service.executor;

import static com.theromman.cats.service.utils.ConsoleCommand.REMOVE_CAT_OWNER;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.utils.ArgumentsSource;
import com.theromman.cats.service.utils.ConsoleCommand;

public class RemoveCatOwnerExecutor extends ExecutorChain{

    @Override
    public void execute(ArgumentsSource argumentsSource) {
        if (argumentsSource.getConsoleCommand() == getSupportedCommand()) {
            CatsApplicationContext.getInstance().getPersonsConsoleAdapter().removeCat(argumentsSource);
        } else executeNext(argumentsSource);
    }

    @Override
    protected ConsoleCommand getSupportedCommand() {
        return REMOVE_CAT_OWNER;
    }
}
