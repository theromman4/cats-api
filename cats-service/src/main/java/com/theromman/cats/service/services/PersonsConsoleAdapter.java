package com.theromman.cats.service.services;

import com.theromman.cats.data.model.Person;
import com.theromman.cats.service.utils.ArgumentsSource;

public interface PersonsConsoleAdapter {
    Person createPerson(ArgumentsSource arguments);
    void deletePerson(ArgumentsSource arguments);
    void removeCat(ArgumentsSource argumentsSource);
    void adoptCat(ArgumentsSource argumentsSource);
}
