package com.theromman.cats.service.executor;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.utils.ArgumentsSource;
import com.theromman.cats.service.utils.ConsoleCommand;

public class UpdateCatExecutor extends ExecutorChain {
    @Override
    public void execute(ArgumentsSource argumentsSource) {
        if (getSupportedCommand() == argumentsSource.getConsoleCommand()) {
            CatsApplicationContext.getInstance().getCatsConsoleAdapter().updateCat(argumentsSource);
        } else {
            executeNext(argumentsSource);
        }
    }

    @Override
    protected ConsoleCommand getSupportedCommand() {
        return ConsoleCommand.UPDATE_CAT;
    }
}
