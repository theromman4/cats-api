package com.theromman.cats.service.services;

import com.theromman.cats.data.model.Color;
import java.util.Optional;

public interface ColorsService {
    Optional<Color> findByColor(String colorName);

    Color save(Color setColor);
}
