package com.theromman.cats.service.utils;

import java.util.Objects;

public enum ConsoleCommand {
    // creation
    CREATE_CAT("create-cat"),
    CREATE_PERSON("create-person"),

    // printing
    PRINT_ALL_CATS("print-all-cats"),
    PRINT_ALL_OWNERS("print-all-owners"),

    // update
    UPDATE_CAT("update-cat"),
    UNFRIEND_CAT("unfriend-cat"),
    CHANGE_OWNER("change-owner"),
    DELETE_CAT("delete-cat"),
    DELETE_PERSON("delete-person"),
    MAKE_FRIEND_CAT("make-friend-cat"),
    REMOVE_CAT_OWNER("remove-cat-owner"),
    ADOPT("adopt"),
    ;
    private final String value;

    ConsoleCommand(String value) {
        this.value = value;
    }

    public static ConsoleCommand findByValue(String value) {
        for (ConsoleCommand command : values()) {
            if (Objects.equals(command.value, value)) {
                return command;
            }
        }
        throw new RuntimeException("Not found com.theromman.cats.aggregator.utils.ConsoleCommand by value: " + value);
    }
}
