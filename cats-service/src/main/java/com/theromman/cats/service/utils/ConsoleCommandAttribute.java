package com.theromman.cats.service.utils;

public enum ConsoleCommandAttribute {
    ID("id"),
    NAME("name"),
    BIRTH_DATE("birthDate"),
    BREED("breed"),
    COLOR("color"),
    OWNER_ID("ownerId"),
    CAT_ID("catId"),
    FRIEND_ID("friendId"),
    FRIENDS("friends"),
    ;

    private final String value;

    ConsoleCommandAttribute(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
