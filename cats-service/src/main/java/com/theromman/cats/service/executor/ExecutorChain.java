package com.theromman.cats.service.executor;

import com.theromman.cats.service.utils.ArgumentsSource;
import com.theromman.cats.service.utils.ConsoleCommand;

public abstract class ExecutorChain {
    private ExecutorChain next;

    public static ExecutorChain init(ExecutorChain firstExecutor, ExecutorChain... executorChain) {
        ExecutorChain head = firstExecutor;
        for (ExecutorChain currentExecutorChain : executorChain) {
            head.next = currentExecutorChain;
            head = currentExecutorChain;
        }
        return firstExecutor;
    }

    public abstract void execute(ArgumentsSource argumentsSource);

    protected void executeNext(ArgumentsSource argumentsSource) {
        if (next != null) {
            next.execute(argumentsSource);
        }
    }

    protected abstract ConsoleCommand getSupportedCommand();
}
