package com.theromman.cats.service.services;

import static com.theromman.cats.service.utils.ConsoleCommandAttribute.BIRTH_DATE;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.BREED;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.CAT_ID;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.COLOR;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.FRIENDS;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.FRIEND_ID;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.ID;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.NAME;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.OWNER_ID;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Person;
import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.utils.ArgumentsSource;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

public class CatsConsoleAdapterImpl implements CatsConsoleAdapter {

    private final CatsService catsService;

    public CatsConsoleAdapterImpl() {
        this.catsService = CatsApplicationContext.getInstance().getCatsService();
    }

    @Override
    public Cat createCat(ArgumentsSource arguments) {
        String name = arguments.getRequiredParameter(NAME);
        Optional<String> colorName = arguments.getOptionalParameter(COLOR);
        Optional<Long> ownerId = arguments.getOptionalParameterLong(OWNER_ID);
        Optional<LocalDate> birthDate = arguments.getOptionalParameterDate(BIRTH_DATE);
        Optional<String> breed = arguments.getOptionalParameter(BREED);
        Optional<String> friendIdsOptional = arguments.getOptionalParameter(FRIENDS);
        var friendIds = friendIdsOptional
            .map(it -> it.split(","))
            .map(it -> Arrays.stream(it).map(Long::parseLong).toList());
        return catsService.createCat(name, colorName, ownerId, birthDate, breed, friendIds);
    }

    @Override
    public Person changeOwner(ArgumentsSource arguments) {
        Long ownerId = arguments.getRequiredParameterLong(OWNER_ID);
        Long catId = arguments.getRequiredParameterLong(CAT_ID);
        return catsService.changeOwner(ownerId, catId);
    }

    @Override
    public void unfriendCat(ArgumentsSource arguments) {
        Long catId = arguments.getRequiredParameterLong(CAT_ID);
        Long friendId = arguments.getRequiredParameterLong(FRIEND_ID);
        catsService.unfriendCat(catId, friendId);
    }

    @Override
    public Cat updateCat(ArgumentsSource arguments) {
        Long id = arguments.getRequiredParameterLong(ID);
        Optional<String> name = arguments.getOptionalParameter(NAME);
        Optional<LocalDate> birthDate = arguments.getOptionalParameterDate(BIRTH_DATE);
        Optional<String> breed = arguments.getOptionalParameter(BREED);
        Optional<Long> ownerId = arguments.getOptionalParameterLong(OWNER_ID);
        return catsService.updateCat(id, name, birthDate, breed, ownerId);
    }

    @Override
    public Cat addOwner(ArgumentsSource arguments) {
        Long catId = arguments.getRequiredParameterLong(CAT_ID);
        Long ownerId = arguments.getRequiredParameterLong(OWNER_ID);
        return catsService.addOwner(catId, ownerId);
    }

    @Override
    public void deleteCat(ArgumentsSource arguments) {
        Long catId = arguments.getRequiredParameterLong(ID);
        catsService.deleteCat(catId);
    }

    @Override
    public void makeFriend(ArgumentsSource arguments) {
        Long catId = arguments.getRequiredParameterLong(CAT_ID);
        Long friendId = arguments.getRequiredParameterLong(FRIEND_ID);
        catsService.makeFriend(catId, friendId);
    }
}
