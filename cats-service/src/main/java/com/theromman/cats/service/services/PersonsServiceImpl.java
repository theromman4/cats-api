package com.theromman.cats.service.services;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Person;
import com.theromman.cats.data.repository.CrudRepository;
import com.theromman.cats.service.context.CatsApplicationContext;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class PersonsServiceImpl implements PersonsService {
    private final CrudRepository<Long, Person> personsRepository;
    private final CrudRepository<Long, Cat> catsRepository;

    public PersonsServiceImpl() {
        this.personsRepository = CatsApplicationContext.getInstance().getPersonsRepository();
        this.catsRepository = CatsApplicationContext.getInstance().getCatsRepository();
    }

    @Override
    public Person createPerson(String name, LocalDate birthDate) {
        Person person = new Person()
                .setName(name)
                .setBirthDate(birthDate)
                ;
        personsRepository.save(person);
        return person;
    }

    @Override
    public void printAllOwners() {
        List<Person> persons = personsRepository.findAll();
        System.out.println(persons);
    }

    @Override
    public void printAllCats(Long id) {
        System.out.println(personsRepository.findById(id).get().getCats());
    }

    @Override
    public void removeCat(Long catId, Long ownerId) {
        var owner = personsRepository.findById(ownerId);

        if (owner.isPresent()) {
            owner.get().getCats().removeIf(cat -> Objects.equals(cat.getId(), catId));
            personsRepository.save(owner.get());
        }
    }

    @Override
    public void addCat(Long catId, Long ownerId) {
        var owner = personsRepository.findById(ownerId);
        var cat = catsRepository.findById(catId);

        if (owner.isPresent() && cat.isPresent()) {
            owner.get().getCats().add(cat.get());
            personsRepository.save(owner.get());
        }
    }

    @Override
    public Person findById(Long id) {
        Person foundPerson = personsRepository
                .findById(id)
                .orElseThrow(() -> new RuntimeException("Person with this id is not found " + id));
        return foundPerson;
    }

    @Override
    public void deletePerson(Long id) {
        personsRepository.delete(findById(id));
    }
}
