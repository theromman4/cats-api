package com.theromman.cats.service.context;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Color;
import com.theromman.cats.data.model.Person;
import com.theromman.cats.data.repository.CrudRepository;
import com.theromman.cats.service.services.CatsConsoleAdapter;
import com.theromman.cats.service.services.CatsConsoleAdapterImpl;
import com.theromman.cats.service.services.CatsService;
import com.theromman.cats.service.services.CatsServiceImpl;
import com.theromman.cats.service.services.ColorsService;
import com.theromman.cats.service.services.ColorsServiceImpl;
import com.theromman.cats.service.services.PersonsConsoleAdapter;
import com.theromman.cats.service.services.PersonsConsoleAdapterImpl;
import com.theromman.cats.service.services.PersonsService;
import com.theromman.cats.service.services.PersonsServiceImpl;

public final class CatsApplicationContext {
    private static CatsApplicationContext INSTANCE;
    private CatsService catsService;
    private PersonsService personsService;
    private ColorsService colorsService;
    private CrudRepository<Long, Cat> catsRepository;
    private CrudRepository<Long, Person> personsRepository;
    private CrudRepository<Long, Color> colorsRepository;
    private CatsConsoleAdapter catsConsoleAdapter;
    private PersonsConsoleAdapter personsConsoleAdapter;

    private CatsApplicationContext() {

    }

    public static CatsApplicationContext getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CatsApplicationContext();
        }
        return INSTANCE;
    }

    public CatsService getCatsService() {
        if (catsService == null) {
            catsService = new CatsServiceImpl();
        }
        return catsService;
    }

    public CrudRepository<Long, Cat> getCatsRepository() {
        if (catsRepository == null) {
            catsRepository = new CrudRepository<>(null, null, Cat.class);
        }
        return catsRepository;
    }

    public PersonsService getPersonsService() {
        if (personsService == null) {
            personsService = new PersonsServiceImpl();
        }
        return personsService;
    }

    public CrudRepository<Long, Person> getPersonsRepository() {
        if (personsRepository == null) {
            personsRepository = new CrudRepository<>(null, null, Person.class);
        }
        return personsRepository;
    }

    public ColorsService getColorsService() {
        if (colorsService == null) {
            colorsService = new ColorsServiceImpl();
        }
        return colorsService;
    }

    public CrudRepository<Long, Color> getColorsRepository() {
        if (colorsRepository == null) {
            colorsRepository = new CrudRepository<>(null, null, Color.class);
        }
        return colorsRepository;
    }

    public CatsConsoleAdapter getCatsConsoleAdapter() {
        if (catsConsoleAdapter == null) {
            catsConsoleAdapter = new CatsConsoleAdapterImpl();
        }
        return catsConsoleAdapter;
    }

    public PersonsConsoleAdapter getPersonsConsoleAdapter() {
        if (personsConsoleAdapter == null) {
            personsConsoleAdapter = new PersonsConsoleAdapterImpl();
        }
        return personsConsoleAdapter;
    }
}
