package com.theromman.cats.service.test_training;

public class MultiplyCalculator {
    private final SquareCalculator squareCalculator;

    public MultiplyCalculator(SquareCalculator squareCalculator) {
        this.squareCalculator = squareCalculator;
    }

    public double calculate(double first, double second) {
        if (first == second) {
            return squareCalculator.calculate(first);
        } else {
            return first * second;
        }
    }
}
