package com.theromman.cats.service.services;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Person;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CatsService {

    Cat createCat(
        String name,
        Optional<String> colorName,
        Optional<Long> ownerId,
        Optional<LocalDate> birthDate,
        Optional<String> breed,
        Optional<List<Long>> friendsIds
    );

    Cat findById(Long id);

    Person changeOwner(Long ownerId, Long catId);

    void unfriendCat(Long catId, Long friendId);

    Cat updateCat(
        Long id,
        Optional<String> name,
        Optional<LocalDate> birthDate,
        Optional<String> breed,
        Optional<Long> ownerId
    );

    void printAllCats();

    Cat addOwner(Long catId, Long ownerId);

    void deleteCat(Long catId);

    void makeFriend(Long catId, Long friendId);

}
