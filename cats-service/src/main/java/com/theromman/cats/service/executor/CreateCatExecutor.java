package com.theromman.cats.service.executor;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.utils.ArgumentsSource;
import com.theromman.cats.service.utils.ConsoleCommand;

public class CreateCatExecutor extends ExecutorChain {
    @Override
    public void execute(ArgumentsSource argumentsSource) {
        if (getSupportedCommand() == argumentsSource.getConsoleCommand()) {
            CatsApplicationContext.getInstance().getCatsConsoleAdapter().createCat(argumentsSource);
        } else {
            executeNext(argumentsSource);
        }
    }

    @Override
    protected ConsoleCommand getSupportedCommand() {
        return ConsoleCommand.CREATE_CAT;
    }
}
