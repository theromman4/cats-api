package com.theromman.cats.service.services;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Person;
import com.theromman.cats.service.utils.ArgumentsSource;

public interface CatsConsoleAdapter {
    Cat createCat(ArgumentsSource arguments);

    Person changeOwner(ArgumentsSource arguments);

    void unfriendCat(ArgumentsSource arguments);

    Cat updateCat(ArgumentsSource arguments);

    Cat addOwner(ArgumentsSource arguments);

    void deleteCat(ArgumentsSource arguments);

    void makeFriend(ArgumentsSource arguments);
}
