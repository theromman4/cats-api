package com.theromman.cats.service.executor;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.services.PersonsServiceImpl;
import com.theromman.cats.service.utils.ArgumentsSource;
import com.theromman.cats.service.utils.ConsoleCommand;

public class PrintAllOwnersExecutor extends ExecutorChain {
    @Override
    public void execute(ArgumentsSource argumentsSource) {
        if (getSupportedCommand() == argumentsSource.getConsoleCommand()) {
            CatsApplicationContext.getInstance().getPersonsService().printAllOwners();
        } else {
            executeNext(argumentsSource);
        }
    }

    @Override
    protected ConsoleCommand getSupportedCommand() {
        return ConsoleCommand.PRINT_ALL_OWNERS;
    }
}
