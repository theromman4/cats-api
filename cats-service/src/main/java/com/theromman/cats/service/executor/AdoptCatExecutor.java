package com.theromman.cats.service.executor;

import static com.theromman.cats.service.utils.ConsoleCommand.ADOPT;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.utils.ArgumentsSource;
import com.theromman.cats.service.utils.ConsoleCommand;

public class AdoptCatExecutor extends ExecutorChain {

    @Override
    public void execute(ArgumentsSource argumentsSource) {
        if (argumentsSource.getConsoleCommand() == getSupportedCommand()) {
            CatsApplicationContext.getInstance().getPersonsConsoleAdapter().adoptCat(argumentsSource);
        } else executeNext(argumentsSource);
    }

    @Override
    protected ConsoleCommand getSupportedCommand() {
        return ADOPT;
    }
}
