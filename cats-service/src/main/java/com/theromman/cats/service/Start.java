package com.theromman.cats.service;

import com.theromman.cats.service.executor.*;
import com.theromman.cats.service.utils.ArgumentsSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Start {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            ExecutorChain executorChain = ExecutorChain.init(
                new ChangeOwnerExecutor(),
                new CreateCatExecutor(),
                new CreatePersonExecutor(),
                new DeleteCatExecutor(),
                new DeletePersonExecutor(),
                new MakeFriendExecutor(),
                new PrintAllCatsExecutor(),
                new PrintAllOwnersExecutor(),
                new UnfriendCatExecutor(),
                new UpdateCatExecutor(),
                new RemoveCatOwnerExecutor(),
                new AdoptCatExecutor()
            );
            while (true) {
                String command = reader.readLine();
                if (command.equals("exit")) {
                    break;
                } else {
                    var argumentsSource = new ArgumentsSource(Arrays.stream(command.split(" ")).toList());
                    executorChain.execute(argumentsSource);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
