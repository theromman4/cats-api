package com.theromman.cats.service.services;

import com.theromman.cats.data.model.Person;
import java.time.LocalDate;

public interface PersonsService {

    Person createPerson(String name, LocalDate birthDate);

    void printAllOwners();

    Person findById(Long id);

    void deletePerson(Long id);

    void printAllCats(Long id);

    void removeCat(Long catId, Long ownerId);

    void addCat(Long catId, Long ownerId);
}
