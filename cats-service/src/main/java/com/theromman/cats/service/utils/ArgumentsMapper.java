package com.theromman.cats.service.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArgumentsMapper {
    public static Map<String, String> listToMap(List<String> arguments) {
        Map<String, String> argumentsMap = new HashMap<>();
        for (String argument : arguments) {
            String[] splittedArgument = argument.split("=");
            if (splittedArgument.length != 2) {
                throw new RuntimeException("Incorrect parameter! " + argument);
            }
            argumentsMap.put(splittedArgument[0], splittedArgument[1]);
        }
        return argumentsMap;
    }
}
