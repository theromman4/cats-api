package com.theromman.cats.service.utils;

import com.theromman.cats.data.model.enums.ColorType;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class ArgumentsSource {
    private final Map<String, String> arguments;
    private final ConsoleCommand consoleCommand;

    public ArgumentsSource(List<String> arguments) {
        consoleCommand = ConsoleCommand.findByValue(arguments.get(0));
        this.arguments = ArgumentsMapper.listToMap(
                arguments.stream().skip(1).toList()
        );
    }

    public ArgumentsSource(Map<String, String> arguments, ConsoleCommand consoleCommand) {
        this.arguments = arguments;
        this.consoleCommand = consoleCommand;
    }

    public ConsoleCommand getConsoleCommand() {
        return consoleCommand;
    }

    public Optional<String> getOptionalParameter(ConsoleCommandAttribute attribute) {
        return Optional
                .ofNullable(arguments.get(attribute.getValue()));
    }

    public Optional<Float> getOptionalParameterFloat(ConsoleCommandAttribute attribute) {
        return getOptionalParameter(attribute).map(Float::parseFloat);
    }

    public Optional<Double> getOptionalParameterDouble(ConsoleCommandAttribute attribute) {
        return getOptionalParameter(attribute).map(Double::parseDouble);
    }

    public Optional<UUID> getOptionalParameterUUID(ConsoleCommandAttribute attribute) {
        return getOptionalParameter(attribute).map(UUID::fromString);
    }

    public Float getOptionalParameterFloatOrDefault(ConsoleCommandAttribute attribute, float defaultValue) {
        var valueFromSource = getOptionalParameterFloat(attribute);
        return valueFromSource.orElse(defaultValue);
    }

    public String getRequiredParameter(ConsoleCommandAttribute attribute) {
        return Optional
                .ofNullable(arguments.get(attribute.getValue()))
                .orElseThrow(() -> new RuntimeException("Required attribute not found in parameters! " + attribute.getValue()));
    }

    public UUID getRequiredParameterUUID(ConsoleCommandAttribute attribute) {
        var uuidString = getRequiredParameter(attribute);
        try {
            return UUID.fromString(uuidString);
        } catch (IllegalArgumentException illegalArgumentException) {
            throw new RuntimeException("Can not parse UUID parameter from command line! Actual value is " + uuidString);
        }
    }

    public long getRequiredParameterLong(ConsoleCommandAttribute attribute) {
        var parameter = getRequiredParameter(attribute);
        try {
            return Long.parseLong(parameter);
        } catch (NumberFormatException numberFormatException) {
            throw new RuntimeException("Incorrect argument! Can not parse " + attribute + "to long with value: " + parameter);
        }
    }

    public double getRequiredParameterDouble(ConsoleCommandAttribute attribute) {
        var parameter = getRequiredParameter(attribute);
        try {
            return Double.parseDouble(parameter);
        } catch (NumberFormatException numberFormatException) {
            throw new RuntimeException("Incorrect argument! Can not parse " + attribute + "to double with value: " + parameter);
        }
    }

    public float getRequiredParameterFloat(ConsoleCommandAttribute attribute) {
        var parameter = getRequiredParameter(attribute);
        try {
            return Float.parseFloat(parameter);
        } catch (NumberFormatException numberFormatException) {
            throw new RuntimeException("Incorrect argument! Can not parse " + attribute + "to float with value: " + parameter);
        }
    }

    public int getRequiredParameterInt(ConsoleCommandAttribute attribute) {
        var parameter = getRequiredParameter(attribute);
        try {
            return Integer.parseInt(parameter);
        } catch (NumberFormatException numberFormatException) {
            throw new RuntimeException("Incorrect argument! Can not parse " + attribute + "to int with value: " + parameter);
        }
    }

    public LocalDate getRequiredParameterDate(ConsoleCommandAttribute dateAttribute) {
        var parameter = getRequiredParameter(dateAttribute);
        try {
            return LocalDate.parse(parameter);
        } catch (DateTimeParseException dateTimeParseException) {
            throw new RuntimeException("Incorrect argument! Can not parse " + dateAttribute + "to date with value: " + parameter);
        }
    }

    public Optional<LocalDate> getOptionalParameterDate(ConsoleCommandAttribute attribute) {
        var attributeValue = getOptionalParameter(attribute);
        return attributeValue.map(LocalDate::parse);
    }

    public ColorType getOptionalParameterColorType(ConsoleCommandAttribute color) {
        try {
            return getOptionalParameter(color).map(ColorType::valueOf).get();
        } catch (RuntimeException e) {
            return ColorType.UNKNOWN;
        }
    }

    public Optional<Long> getOptionalParameterLong(ConsoleCommandAttribute attribute) {
        return getOptionalParameter(attribute).map(Long::parseLong);
    }
}
