package com.theromman.cats.service.executor;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.utils.ArgumentsSource;
import com.theromman.cats.service.utils.ConsoleCommand;

public class CreatePersonExecutor extends ExecutorChain {
    @Override
    public void execute(ArgumentsSource argumentsSource) {
        if (getSupportedCommand() == argumentsSource.getConsoleCommand()) {
            CatsApplicationContext.getInstance().getPersonsConsoleAdapter().createPerson(argumentsSource);
        } else {
            executeNext(argumentsSource);
        }
    }

    @Override
    protected ConsoleCommand getSupportedCommand() {
        return ConsoleCommand.CREATE_PERSON;
    }
}
