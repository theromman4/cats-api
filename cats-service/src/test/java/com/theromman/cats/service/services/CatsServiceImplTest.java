package com.theromman.cats.service.services;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Color;
import com.theromman.cats.data.repository.CrudRepository;
import com.theromman.cats.service.context.CatsApplicationContext;
import java.time.LocalDate;
import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.MockedStatic;

@TestInstance(Lifecycle.PER_CLASS)
class CatsServiceImplTest {

    private final String name = "васька";
    private final String colorName = "WHITE";
    private final Color color = new Color();
    private final Long ownerId = null;
    private final LocalDate birthDate = LocalDate.parse("2017-02-23");
    private final String breed = "сфинкс";
    private final Cat expectedCat = new Cat()
        .setName(name)
        .setBirthDate(birthDate)
        .setBreed(breed)
        .setColor(color)
        .setOwner(null)
        .setFriends(null);

    private MockedStatic<CatsApplicationContext> contextMock;
    private final CatsApplicationContext mockCatsApplicationContext = mock(CatsApplicationContext.class);
    private final CrudRepository<Long, Cat> catsRepositoryMock = mock(CrudRepository.class);
    private final ColorsService colorsServiceMock = mock(ColorsService.class);
    private final PersonsService personsServiceMock = mock(PersonsService.class);
    private CatsService sut;

    @BeforeAll
    void beforeAll() {
        contextMock = mockStatic(CatsApplicationContext.class);
        contextMock.when(CatsApplicationContext::getInstance).thenReturn(mockCatsApplicationContext);
        when(mockCatsApplicationContext.getCatsRepository()).thenReturn(catsRepositoryMock);
        when(mockCatsApplicationContext.getColorsService()).thenReturn(colorsServiceMock);
        when(mockCatsApplicationContext.getPersonsService()).thenReturn(personsServiceMock);

        sut = new CatsServiceImpl();

        when(colorsServiceMock.findByColor(colorName)).thenReturn(Optional.of(color));
        when(catsRepositoryMock.save(any())).thenReturn(expectedCat);
    }

    @AfterAll
    void afterAll() {
        contextMock.close();
    }

    @Test
    void createCat() {
        var actual = sut.createCat(name, Optional.of(colorName), Optional.ofNullable(ownerId),
            Optional.of(birthDate), Optional.of(breed), Optional.empty()
        );

        verify(colorsServiceMock).findByColor(colorName);
        verify(personsServiceMock, times(0)).findById(any());
        verify(catsRepositoryMock).save(any());
        assertAll(
            "Test expected and actual cat equality",
            () -> assertEquals(expectedCat.getName(), actual.getName()),
            () -> assertEquals(expectedCat.getBirthDate(), actual.getBirthDate()),
            () -> assertEquals(expectedCat.getBreed(), actual.getBreed()),
            () -> assertEquals(expectedCat.getColor(), actual.getColor()),
            () -> assertEquals(expectedCat.getFriends(), actual.getFriends()),
            () -> assertEquals(expectedCat.getOwner(), actual.getOwner())
        );
    }
}