package com.theromman.cats.service.services;

import static com.theromman.cats.service.utils.ConsoleCommandAttribute.BIRTH_DATE;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.BREED;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.CAT_ID;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.COLOR;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.FRIEND_ID;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.NAME;
import static com.theromman.cats.service.utils.ConsoleCommandAttribute.OWNER_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Person;
import com.theromman.cats.data.model.enums.ColorType;
import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.utils.ArgumentsSource;
import com.theromman.cats.service.utils.ConsoleCommand;
import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.MockedStatic;

@TestInstance(Lifecycle.PER_CLASS)
class CatsConsoleAdapterImplTest {

    private final Long catId = 2L;
    private final Long otherCatId = 5L;
    private final Long ownerId = 3L;
    private final String name = "барсик";
    private final LocalDate birthDate = LocalDate.parse("2015-01-02");
    private final String breed = "абиссинская";
    private final String colorName = "WHITE";
    private final ColorType colorType = ColorType.WHITE;
    private final Cat cat = new Cat().setName(name).setBirthDate(birthDate).setBreed(breed);
    private final Person person = new Person().setId(ownerId);
    private final ArgumentsSource createCatArgumentsSource = createArgumentsSourceCreateCat();
    private final ArgumentsSource changeOwnerArgumentsSource = createArgumentsSourceChangeOwner();
    private final ArgumentsSource unfriendCatArgumentsSource = createArgumentsSourceUnfriendCat();

    private MockedStatic<CatsApplicationContext> contextMock;
    private final CatsService catsServiceMock = mock(CatsService.class);
    private final CatsApplicationContext catsApplicationContextMock = mock(CatsApplicationContext.class);
    private CatsConsoleAdapter sut;

    @BeforeAll
    void beforeAll() {
        contextMock = mockStatic(CatsApplicationContext.class);
        contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
        when(catsApplicationContextMock.getCatsService()).thenReturn(catsServiceMock);
        when(catsServiceMock.findById(catId)).thenReturn(cat);

        sut = new CatsConsoleAdapterImpl();
    }

    @AfterAll
    void afterAll() {
        contextMock.close();
    }

    @Test
    public void createCat() {
        when(catsServiceMock.createCat(any(), any(), any(), any(), any(), any())).thenReturn(cat);

        var actual = sut.createCat(createCatArgumentsSource);

        verify(catsServiceMock).createCat(
            name,
            Optional.of(colorName),
            Optional.empty(),
            Optional.of(birthDate),
            Optional.of(breed),
            Optional.empty());
        assertEquals(cat, actual);
    }

    @Test
    public void changeOwner() {
        when(catsServiceMock.changeOwner(ownerId, catId)).thenReturn(person);

        var actual = sut.changeOwner(changeOwnerArgumentsSource);

        verify(catsServiceMock).changeOwner(ownerId, catId);
        assertEquals(person, actual);
    }

    @Test
    public void unfriendCat() {
        doNothing().when(catsServiceMock).unfriendCat(catId, otherCatId);

        sut.unfriendCat(unfriendCatArgumentsSource);

        verify(catsServiceMock).unfriendCat(catId, otherCatId);
    }

    private ArgumentsSource createArgumentsSourceCreateCat() {
        return new ArgumentsSource(
            Map.of(
                NAME.getValue(), name,
                COLOR.getValue(), colorName,
                BIRTH_DATE.getValue(), birthDate.toString(),
                BREED.getValue(), breed
            ),
            ConsoleCommand.CREATE_CAT
        );
    }

    private ArgumentsSource createArgumentsSourceChangeOwner() {
        return new ArgumentsSource(
            Map.of(
                CAT_ID.getValue(), catId.toString(),
                OWNER_ID.getValue(), ownerId.toString()
            ),
            ConsoleCommand.CHANGE_OWNER
        );
    }

    private ArgumentsSource createArgumentsSourceUnfriendCat() {
        return new ArgumentsSource(
            Map.of(
                CAT_ID.getValue(), catId.toString(),
                FRIEND_ID.getValue(), otherCatId.toString()
            ),
            ConsoleCommand.UNFRIEND_CAT
        );
    }
}