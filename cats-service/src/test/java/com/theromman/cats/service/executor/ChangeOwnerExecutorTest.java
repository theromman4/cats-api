package com.theromman.cats.service.executor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.services.CatsConsoleAdapter;
import com.theromman.cats.service.utils.ArgumentsSource;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

class ChangeOwnerExecutorTest {

    private final ArgumentsSource argumentsSourceForCurrent = new ArgumentsSource(List.of("change-owner"));
    private final ArgumentsSource argumentsSourceForAnother = new ArgumentsSource(List.of("create-cat"));

    private final CatsConsoleAdapter catsConsoleAdapterMock = mock(CatsConsoleAdapter.class);
    private final CatsApplicationContext catsApplicationContextMock = mock(CatsApplicationContext.class);

    @Test
    public void testForCurrent() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            ChangeOwnerExecutor sut = new ChangeOwnerExecutor();

            when(catsApplicationContextMock.getCatsConsoleAdapter()).thenReturn(catsConsoleAdapterMock);

            sut.execute(argumentsSourceForCurrent);

            verify(catsConsoleAdapterMock).changeOwner(argumentsSourceForCurrent);
        }
    }

    @Test
    public void testForAnother() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            ChangeOwnerExecutor sut = new ChangeOwnerExecutor();

            when(catsApplicationContextMock.getCatsConsoleAdapter()).thenReturn(catsConsoleAdapterMock);

            sut.execute(argumentsSourceForAnother);

            verify(catsConsoleAdapterMock, times(0)).changeOwner(any());
        }
    }
}