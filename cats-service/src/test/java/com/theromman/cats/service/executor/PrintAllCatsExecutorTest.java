package com.theromman.cats.service.executor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.services.CatsService;
import com.theromman.cats.service.utils.ArgumentsSource;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

class PrintAllCatsExecutorTest {
    private final ArgumentsSource argumentsSourceForCurrent = new ArgumentsSource(List.of("print-all-cats"));
    private final ArgumentsSource argumentsSourceForAnother = new ArgumentsSource(List.of("adopt"));

    private final CatsService catsServiceMock = mock(CatsService.class);
    private final CatsApplicationContext catsApplicationContextMock = mock(CatsApplicationContext.class);

    @Test
    public void testForCurrent() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            PrintAllCatsExecutor sut = new PrintAllCatsExecutor();

            when(catsApplicationContextMock.getCatsService()).thenReturn(catsServiceMock);

            sut.execute(argumentsSourceForCurrent);

            verify(catsServiceMock).printAllCats();
        }
    }

    @Test
    public void testForAnother() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            PrintAllCatsExecutor sut = new PrintAllCatsExecutor();

            when(catsApplicationContextMock.getCatsService()).thenReturn(catsServiceMock);

            sut.execute(argumentsSourceForAnother);

            verify(catsServiceMock, times(0)).printAllCats();
        }
    }
}