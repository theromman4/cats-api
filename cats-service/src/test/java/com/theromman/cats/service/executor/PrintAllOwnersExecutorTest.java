package com.theromman.cats.service.executor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.services.PersonsService;
import com.theromman.cats.service.utils.ArgumentsSource;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

class PrintAllOwnersExecutorTest {
    private final ArgumentsSource argumentsSourceForCurrent = new ArgumentsSource(List.of("print-all-owners"));
    private final ArgumentsSource argumentsSourceForAnother = new ArgumentsSource(List.of("adopt"));

    private final PersonsService personsServiceMock = mock(PersonsService.class);
    private final CatsApplicationContext catsApplicationContextMock = mock(CatsApplicationContext.class);

    @Test
    public void testForCurrent() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            PrintAllOwnersExecutor sut = new PrintAllOwnersExecutor();

            when(catsApplicationContextMock.getPersonsService()).thenReturn(personsServiceMock);

            sut.execute(argumentsSourceForCurrent);

            verify(personsServiceMock).printAllOwners();
        }
    }

    @Test
    public void testForAnother() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            PrintAllOwnersExecutor sut = new PrintAllOwnersExecutor();

            when(catsApplicationContextMock.getPersonsService()).thenReturn(personsServiceMock);

            sut.execute(argumentsSourceForAnother);

            verify(personsServiceMock, times(0)).printAllOwners();
        }
    }
}