package com.theromman.cats.service.utils;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ArgumentsMapperTest {
    private final String property1 = "property1";
    private final String value1 = "value1";
    private final String property2 = "property2";
    private final String value2 = "value2";

    @Test
    public void listToMap_ok() {
        var expected = Map.of(property1, value1, property2, value2);

        var actual = ArgumentsMapper.listToMap(List.of(property1 + "=" + value1, property2 + "=" + value2));

        assertEquals(expected, actual);
    }

    @Test
    public void listToMap_incorrectSource() {
        var expected = "Incorrect parameter! " + property1 + value1;

        var actual = assertThrows(RuntimeException.class,
                                  () -> ArgumentsMapper.listToMap(List.of(property1 + value1)));

        assertEquals(expected, actual.getMessage());
    }
}