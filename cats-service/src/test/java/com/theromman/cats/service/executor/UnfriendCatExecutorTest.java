package com.theromman.cats.service.executor;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.services.CatsConsoleAdapter;
import com.theromman.cats.service.utils.ArgumentsSource;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

class UnfriendCatExecutorTest {
    private final ArgumentsSource argumentsSourceForCurrent = new ArgumentsSource(List.of("unfriend-cat"));
    private final ArgumentsSource argumentsSourceForAnother = new ArgumentsSource(List.of("adopt"));

    private final CatsConsoleAdapter catsConsoleAdapterMock = mock(CatsConsoleAdapter.class);
    private final CatsApplicationContext catsApplicationContextMock = mock(CatsApplicationContext.class);

    @Test
    public void testForCurrent() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            UnfriendCatExecutor sut = new UnfriendCatExecutor();

            when(catsApplicationContextMock.getCatsConsoleAdapter()).thenReturn(catsConsoleAdapterMock);

            sut.execute(argumentsSourceForCurrent);

            verify(catsConsoleAdapterMock).unfriendCat(argumentsSourceForCurrent);
        }
    }

    @Test
    public void testForAnother() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            UnfriendCatExecutor sut = new UnfriendCatExecutor();

            when(catsApplicationContextMock.getCatsConsoleAdapter()).thenReturn(catsConsoleAdapterMock);

            sut.execute(argumentsSourceForAnother);

            verify(catsConsoleAdapterMock, times(0)).unfriendCat(any());
        }
    }
}