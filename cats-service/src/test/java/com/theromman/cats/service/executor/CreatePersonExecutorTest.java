package com.theromman.cats.service.executor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.theromman.cats.service.context.CatsApplicationContext;
import com.theromman.cats.service.services.PersonsConsoleAdapter;
import com.theromman.cats.service.utils.ArgumentsSource;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

class CreatePersonExecutorTest {
    private final ArgumentsSource argumentsSourceForCurrent = new ArgumentsSource(List.of("create-person"));
    private final ArgumentsSource argumentsSourceForAnother = new ArgumentsSource(List.of("adopt"));

    private final PersonsConsoleAdapter personsConsoleAdapterMock = mock(PersonsConsoleAdapter.class);
    private final CatsApplicationContext catsApplicationContextMock = mock(CatsApplicationContext.class);

    @Test
    public void testForCurrent() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            CreatePersonExecutor sut = new CreatePersonExecutor();

            when(catsApplicationContextMock.getPersonsConsoleAdapter()).thenReturn(personsConsoleAdapterMock);

            sut.execute(argumentsSourceForCurrent);

            verify(personsConsoleAdapterMock).createPerson(argumentsSourceForCurrent);
        }
    }

    @Test
    public void testForAnother() {
        try (MockedStatic<CatsApplicationContext> contextMock = mockStatic(CatsApplicationContext.class)) {
            contextMock.when(CatsApplicationContext::getInstance).thenReturn(catsApplicationContextMock);
            CreatePersonExecutor sut = new CreatePersonExecutor();

            when(catsApplicationContextMock.getPersonsConsoleAdapter()).thenReturn(personsConsoleAdapterMock);

            sut.execute(argumentsSourceForAnother);

            verify(personsConsoleAdapterMock, times(0)).createPerson(any());
        }
    }
}