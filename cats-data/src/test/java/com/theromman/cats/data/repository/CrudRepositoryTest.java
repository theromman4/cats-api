package com.theromman.cats.data.repository;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Color;
import com.theromman.cats.data.model.enums.ColorType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CrudRepositoryTest {
    private final Long catId = 1L;
    private final Color whiteColor = new Color().setColor(ColorType.WHITE);
    private final Cat cat = new Cat(catId, "барсик", LocalDate.parse("2015-01-02"),
            "абиссинская", whiteColor, null, null, null);

    private final EntityManagerFactoryObject mockEntityManagerFactoryObject = mock(EntityManagerFactoryObject.class);
    private final EntityManager mockEntityManager = mock(EntityManager.class);
    private final EntityTransaction mockEntityTransaction = mock(EntityTransaction.class);

    private final CrudRepository<Long, Cat> sut = new CrudRepository<>(
        mockEntityManager,
        mockEntityManagerFactoryObject,
        Cat.class
    );

    @BeforeEach
    void beforeEach() {
        when(mockEntityManager.getTransaction()).thenReturn(mockEntityTransaction);
        when(mockEntityManager.find(Cat.class, catId)).thenReturn(cat);
    }

    @Test
    void save() {
        sut.save(cat);

        verify(mockEntityManager).getTransaction();
        verify(mockEntityTransaction).begin();
        verify(mockEntityManager).persist(cat);
        verify(mockEntityTransaction).commit();
    }

    @Test
    void findById() {
        var actual = sut.findById(catId);

        verify(mockEntityManager).getTransaction();
        verify(mockEntityTransaction).begin();
        verify(mockEntityManager).find(Cat.class, catId);
        verify(mockEntityTransaction).commit();

        assertEquals(Optional.of(cat), actual);
    }
}