package com.theromman.cats.data.repository;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactoryObject {

    public EntityManagerFactory createCats() {
        return Persistence.createEntityManagerFactory("Cats");
    }
}
