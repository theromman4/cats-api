package com.theromman.cats.data.model.enums;

public enum ColorType {
    BLACK,  WHITE, TURTLE, UNKNOWN
}
