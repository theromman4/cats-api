package com.theromman.cats.data.repository;

import java.lang.reflect.InvocationTargetException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.Optional;

public class CrudRepository<ID, ENTITY> implements Repository<ID, ENTITY> {
    private final EntityManagerFactory emf = new EntityManagerFactoryObject().createCats();
    private EntityManagerFactoryObject emfo;
    private EntityManager em;
    private Class<ENTITY> clazz;

    public CrudRepository(EntityManager em, EntityManagerFactoryObject emfo, Class<ENTITY> clazz) {
        if (em == null){
            this.em = emf.createEntityManager();
        } else this.em = em;
        if (emfo == null) {
            this.emfo = new EntityManagerFactoryObject();
        } else this.emfo = emfo;
        this.clazz = clazz;
    }

    public ENTITY save(ENTITY entity) {
        var transaction = em.getTransaction();
        transaction.begin();
        em.persist(entity);
        transaction.commit();
        return entity;
    }

    public ENTITY delete(ENTITY entity) {
        var transaction = em.getTransaction();
        transaction.begin();
        em.remove(entity);
        transaction.commit();
        return entity;
    }

    public Optional<ENTITY> findById(ID id) {
        var transaction = em.getTransaction();
        transaction.begin();
        ENTITY entity = em.find(clazz, id);
        transaction.commit();
        return entity != null ? Optional.of(entity) : Optional.empty();
    }

    public List<ENTITY> findAll() {
        var transaction = em.getTransaction();
        transaction.begin();
        List<ENTITY> entities = em.createQuery("select t from " + clazz.getSimpleName() + " t").getResultList();
        transaction.commit();
        return entities;
    }

    public List<ENTITY> findAllPaged(int limit, int offset) {
        var transaction = em.getTransaction();
        transaction.begin();
        List<ENTITY> entities = em.createQuery("select t from " + clazz.getSimpleName() + " t")
                .setMaxResults(limit)
                .setFirstResult(offset)
                .getResultList();
        transaction.commit();
        return entities;
    }

    public Optional<ENTITY> findByFieldValue(String field, String value) {
        List<ENTITY> entities = findAll();
        var result = entities.stream().filter(it -> getIsEntityWithFieldValue(it, field, value)).toList();
        if (result.size() != 1) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(result.get(0));
        }
    }

    private boolean getIsEntityWithFieldValue(ENTITY entity, String field, String value) {
        try {
            var method = entity.getClass()
                .getMethod("get" + field.substring(0, 1).toUpperCase() + field.substring(1));
            var result =  method.invoke(entity);
            if (result instanceof Enum) {
                return ((Enum<?>) result).name().equals(value);
            } else return result.equals(value);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException("Can not find entity with field " + field + " value: " + value, e);
        }
    }
}
