package com.theromman.cats.data.repository;

import java.util.Optional;

public interface Repository<K, V> {
    V save(V object);
    Optional<V> findById(K id);
}
