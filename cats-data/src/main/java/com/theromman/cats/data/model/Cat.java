package com.theromman.cats.data.model;

import java.util.ArrayList;
import javax.persistence.CascadeType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "cats")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode
@ToString
public class Cat {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    private String breed;

    @ManyToOne(cascade = { CascadeType.MERGE })
    @JoinColumn(name = "color_id")
    private Color color;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Person owner;

    @ManyToMany
    @JoinTable(name = "cat_friends",
            joinColumns = @JoinColumn(name = "cat_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "other_cat_id", referencedColumnName = "id")
    )
    @ToString.Exclude
    private List<Cat> friends = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "cat_friends",
        joinColumns = @JoinColumn(name = "other_cat_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "cat_id", referencedColumnName = "id")
    )
    @ToString.Exclude
    private List<Cat> friendOf = new ArrayList<>();
}
