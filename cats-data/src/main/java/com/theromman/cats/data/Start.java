package com.theromman.cats.data;

import com.theromman.cats.data.model.Cat;
import com.theromman.cats.data.model.Color;
import com.theromman.cats.data.repository.CrudRepository;
import java.time.LocalDate;

public class Start {
    public static void main(String[] args) {
//        CrudRepository<Long, Cat> catsRepository = new CrudRepository<>(null, null, Cat.class);
//        System.out.println(catsRepository.findAllPaged(2, 2));
//        createCat();
    }

    private static void createCat() {

        CrudRepository<Long, Color> colorsRepository = new CrudRepository<>(null, null, Color.class);
        var whiteColor = colorsRepository.findByFieldValue("color", "WHITE");
        CrudRepository<Long, Cat> catsRepository = new CrudRepository<>(null, null, Cat.class);
        Cat cat = new Cat();
        cat.setName("барсик");
        cat.setBirthDate(LocalDate.parse("2015-01-02"));
        cat.setBreed("абиссинская");
        whiteColor.map(cat::setColor);
        var savedCat = catsRepository.save(cat);
    }
}
