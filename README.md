Сервис по учету котиков и их владельцев.<br>
<h3>Существующая информация о котиках:</h3>
- Имя<br>
- Дата рождения <br>
- Порода <br>
- Цвет (один из заранее заданных вариантов) <br>
- Хозяин <br>
- Список котиков, с которыми дружит этот котик (из представленных в базе) <br>
<h3>Существующая информация о хозяевах: </h3>
- Имя<br>
- Дата рождения<br>
- Список котиков <br>
Сервис реализует архитектуру controller-service-dao. Вся информация хранится в БД PostgreSQL. 
Для связи с БД используется Hibernate. <br>
Проект собирается с помощью Maven. <br>
Слой доступа к данным и сервисный слой - это два разных модуля Maven. <br>
Для тестирования используется Mockito, Junit 5.

```
create-person name=петя birthDate=2000-01-02
create-person name=вася birthDate=1999-01-02
print-all-owners
create-cat name=мурка birthDate=2015-01-02 breed=sphinx color=WHITE ownerId=19
print-all-cats
create-cat name=пушок birthDate=2014-03-06 breed=sphinx color=BLACK ownerId=20 friends=36
print-all-cats
remove-cat-owner catId=45 ownerId=20
adopt catId=45 ownerId=20
update-cat id=45 name=Пушок
update-cat id=45 ownerId=11
print-all-cats
change-owner catId=45 ownerId=20
print-all-owners
unfriend-cat catId=45 friendId=36
make-friend-cat catId=45 friendId=36
delete-cat id=45
print-all-cats
delete-person id=20

```